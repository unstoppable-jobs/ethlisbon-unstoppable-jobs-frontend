declare global {
  namespace NodeJS {
    interface ProcessEnv {
      NETWORK: string;
      WEB3_MODAL_PROJECT_ID: string;
      CONTRACT_ADDRESS: string;
      GOERLI_RPC_URL: string;
    }
  }
}

export {};
