import React from "react";
import ReactDOM from "react-dom/client";
import { ToastContainer } from "react-toastify";
import { BrowserRouter } from "react-router-dom";
import { ModalProvider } from "./components/modal/Modal.component";
import { AppComponent } from "./components/app/App.component";
import "react-toastify/dist/ReactToastify.css";
import "../public/assets/styles/styles.scss";
import { MenuProvider } from "./hooks/useMenu";
import type { ConfigOptions } from "@web3modal/core";
import { web3ModalProjectId } from "./config/environment.config";
import { Web3Modal } from "@web3modal/react";
import { chains, Chain, providers } from "@web3modal/ethereum";
// import { BlockchainProvider } from "./context/blockchain.context";

// const localhostLisbon: Chain = {
//   id: 1337,
//   name: "LocalhostLisbon",
//   network: "localhostlisbon",
//   nativeCurrency: {
//     decimals: 18,
//     name: "ETH",
//     symbol: "ETH",
//   },
//   rpcUrls: {
//     default: "http://127.0.0.1:8545/",
//   },
//   blockExplorers: {
//     default: { name: "Gnosis Scan Explorer", url: "https://gnosisscan.io" },
//   },
//   testnet: true,
// };

const config: ConfigOptions = {
  projectId: web3ModalProjectId,
  theme: "light",
  accentColor: "default",
  ethereum: {
    appName: "web3Modal",
    autoConnect: true,
    chains: [chains.goerli],
  },
};

const root = ReactDOM.createRoot(
  document.getElementById("mainBody") as HTMLElement
);
root.render(
  <BrowserRouter>
    <MenuProvider>
      <ModalProvider>
        <AppComponent />
        <Web3Modal config={config} />
      </ModalProvider>
    </MenuProvider>
    <ToastContainer />
  </BrowserRouter>
);
