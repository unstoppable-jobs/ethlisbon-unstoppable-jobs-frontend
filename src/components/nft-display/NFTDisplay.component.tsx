import React, { FunctionComponent, ReactNode } from "react";
import cx from "classnames";

import { SpinnerComponent } from "../spinner/Spinner.component";

import styles from "./NFTDisplay.module.scss";

interface ButtonProps {
  color?: "standard" | "green";
  className?: string;
  icon?: string;
  isHoverAnimation?: boolean;
  loading?: boolean;
}

export const NFTDisplayComponent: FunctionComponent<ButtonProps> = (
  props: ButtonProps
): JSX.Element => {
  return (
    <div className={styles["nft-display"]}>
      <div className={styles["nft-display__content"]}>NFTS DISPLAY</div>
    </div>
  );
};
