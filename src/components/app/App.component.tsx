import React from "react";
import { HeaderComponent } from "../nav/header/Header.component";
import { PageContainer } from "../nav/page-container/PageContainer.component";
import { pages } from "../../pages/pages";
import { useMenu } from "../../hooks/useMenu";

export const AppComponent = (): JSX.Element => {
  const { selectedPage } = useMenu();

  return (
    <main>
      <HeaderComponent title="Unstoppable Jobs"/>
      <PageContainer
        selectedPage={selectedPage}
        pages={pages}
      />
    </main>
  );
};
