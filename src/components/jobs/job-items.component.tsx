/* eslint-disable max-len */
import React from "react";
import styles from './job-items.module.scss';

export interface Job {
  image?: string;
  title: string;
  description?: string;
  skills?: string[]
}

interface JobsProps {
  items: Job[] | undefined;
}

export const JobItemsComponent: React.FC<JobsProps> = ({
  items
}) => {
  return (
    <div className={styles["job-items"]}>
      {items && items.map((item: Job) => {
          return (
            <div key={item.title} className={styles["row"]}>
              <div className={styles["image-wrapper"]}>
                <img className={styles["image"]} src={item.image} alt={item.title}/>
              </div>
              <div className={styles["text"]}>
                <h4>{item.title}</h4>
                <p>
                  {item.description}
                </p>
                {item.skills && item.skills.map((skill: string) => {
                  return (
                    <div key={skill} className={styles["skill"]}>
                      <p className={styles["skill-text"]}>{skill}</p>
                    </div>
                  );
                })
                }
              </div>
            </div>
          );
        })
      }
    </div>
  );
};
