import React from 'react';

interface TabContainerProps {
  pages: Page[];
  selectedPage: PageKeys;
}

import styles from './PageContainer.module.scss';
import { Page, PageKeys } from '../../../pages/pages';

export const PageContainer: React.FC<TabContainerProps> = (props: TabContainerProps) => {
  const page = props.pages.find((page) => props.selectedPage === page.key);
  if (page) {
    return (
      <div className={styles['page']}>
        <div className={styles['page-header']}>
          <div className={styles['page-icon-wrapper']}>
            <img
                id="page-icon"
                alt="icon"
                className={styles["page-icon"]}
                src={page.icon}
              />
          </div>
          <h2 className={styles["page-title"]}>{page.title}</h2>
        </div>
        {page.component}
      </div>
    );
  }
  return null;
};
