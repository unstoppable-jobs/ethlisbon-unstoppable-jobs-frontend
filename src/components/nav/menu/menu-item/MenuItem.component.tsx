import React, {FunctionComponent} from 'react';
import cx from 'classnames';
import styles from './MenuItem.module.scss';
import { PageKeys } from '../../../../pages/pages';
import { useMenu } from '../../../../hooks/useMenu';

export interface DropDown {
  index: number;
  open: boolean;
  renderArrow: boolean;
  itemsFunction: () => JSX.Element;
}

export interface MenuItemProps {
  title: string;
  icon: JSX.Element | false;
  collapsible: DropDown | false;
  disabled: boolean;
  pageKey?: PageKeys;
}

export const MenuItemComponent: FunctionComponent<MenuItemProps> = ({
  title,
  icon,
  disabled,
  pageKey,
}: MenuItemProps): JSX.Element => {
  const { onPageSelected } = useMenu();

  const onMenuItemTriggered = (): void => {
    if (pageKey) {
      onPageSelected(pageKey);
    }
  };

  return (
    <li
      className={cx(styles['menu-item'], {
        [styles['disabled'] as string]: disabled,
        [styles['menu-item-active'] as string]: open
      })}
      onClick={onMenuItemTriggered}
      onKeyPress={onMenuItemTriggered}
      role='menuitem'
    >
      <button
            className={cx(styles['button-menu'], {
              [styles['disabled'] as string]: disabled,
            })}
      >
        {icon && icon}
        {title}
      </button>
    </li>
  );
};
