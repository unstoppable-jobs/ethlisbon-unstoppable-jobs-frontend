import React, { FunctionComponent } from "react";
import { Web3Button } from '@web3modal/react';
import styles from "./Menu.module.scss";

import { HamburgerComponent } from "./hamburger/Hamburger.component";
import { useMenu } from "../../../hooks/useMenu";
import {
  MenuItemComponent,
} from "./menu-item/MenuItem.component";
import { mainMenu, MenuItem } from "./MainMenu";

export const MenuComponent: FunctionComponent = (): JSX.Element => {
  const { isHamburgerOpen } = useMenu();

  return (
    <>
      <ul
        className={[
          styles["menu"], isHamburgerOpen ? styles["visible"] : "",
        ].join(" ")}>
        { mainMenu
          .map((item: MenuItem) => {
            return (
              <MenuItemComponent
                key={item.index}
                title={item.title}
                icon={item.icon}
                collapsible={item.collapsible}
                disabled={item.disabled}
                pageKey={item.pageKey}
              />
            );
          })
        }
      </ul>
      <Web3Button/>
      <HamburgerComponent/>
    </>
  );
};
