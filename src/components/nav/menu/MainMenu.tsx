import { PageKeys } from "../../../pages/pages";
import { MenuItemProps } from "./menu-item/MenuItem.component";

export interface MenuItem extends MenuItemProps {
  index: number;
}

export const mainMenu: MenuItem[] = [
  {
    index: 1,
    title: "Home",
    icon: false,
    disabled: false,
    collapsible: false,
    pageKey: PageKeys.Home,
  },
  {
    index: 2,
    title: "Jobs",
    icon: false,
    disabled: false,
    collapsible: false,
    pageKey: PageKeys.Jobs,
  },
  // {
  //   index: 3,
  //   title: "Info",
  //   icon: false,
  //   disabled: false,
  //   collapsible: false,
  //   pageKey: PageKeys.Info,
  // },
  {
    index: 4,
    title: "Personal",
    icon: false,
    disabled: false,
    collapsible: false,
    pageKey: PageKeys.Personal,
  },
];
