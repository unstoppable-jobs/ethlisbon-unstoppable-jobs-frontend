import React, { FunctionComponent } from "react";
import cx from 'classnames';
import { MenuComponent } from "../menu/Menu.component";

import styles from "./Header.module.scss";
import Unstoppable from "../../../../public/assets/images/unstoppable.png";

interface HeaderProps {
  title: string;
}

export const HeaderComponent: FunctionComponent<HeaderProps> = (
  props: HeaderProps
): JSX.Element => {
  return (
    <header>
      <div className={styles["header-wrapper"]}>
        <div className={cx(styles["header-logo-wrapper"], styles["outline-outward"])}>
          <img
            id="header-logo"
            alt="header-logo"
            className={styles["header-logo"]}
            src={Unstoppable}
          />
        </div>
        <div className={styles["header-title"]}>{props.title}</div>
        <MenuComponent/>
      </div>
    </header>
  );
};
