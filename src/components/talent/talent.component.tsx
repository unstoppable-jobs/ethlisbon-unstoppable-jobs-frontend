/* eslint-disable max-len */
import React from "react";
import styles from './talent.module.scss';

export interface Talent {
  image?: string;
  title: string;
  description?: string;
  skills?: string[]
}

interface TalentProps {
  items: Talent[] | undefined;
}

export const TalentComponent: React.FC<TalentProps> = ({
  items
}) => {
  return (
    <div className={styles["talent"]}>
      {items && items.map((item: Talent) => {
          return (
            <div key={item.title} className={styles["row"]}>
              <div className={styles["image-wrapper"]}>
                <img className={styles["image"]} src={item.image} alt={item.title}/>
              </div>
              <div className={styles["text"]}>
                <h4>{item.title}</h4>
                <p>
                  {item.description}
                </p>
                {item.skills && item.skills.map((skill: string) => {
                  return (
                    <div key={skill} className={styles["skill"]}>
                      <p className={styles["skill-text"]}>{skill}</p>
                    </div>
                  );
                })
                }
              </div>
            </div>
          );
        })
      }
    </div>
  );
};
