import React, {
  ReactNode,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import ReactDOM from "react-dom";

import close from "../../../public/assets/images/svg/close.svg";

import styles from "./Modal.module.scss";

interface ModalProps {
  title: string;
  onClose: (outsideClick: boolean) => void;
  options?: ModalOptions;
  children?: ReactNode | undefined;
}

export interface GenericModalProps {
  onClose?: () => void;
  onSuccess?: () => void;
}

export const ModalComponent: React.FC<ModalProps> = ({
  title,
  onClose,
  options,
  children,
}: ModalProps) => {
  const contentRef = useRef<HTMLDivElement>(null);

  const onDocumentClick = useCallback(
    (event: Event) => {
      if (
        contentRef.current &&
        event.target &&
        event.target instanceof Element &&
        !contentRef.current.contains(event.target) &&
        !options?.persistent
      ) {
        onClose(true);
      }
    },
    [contentRef, onClose, options]
  );

  useEffect(() => {
    setTimeout(() => {
      document.addEventListener("click", onDocumentClick);
    });

    return (): void => {
      document.removeEventListener("click", onDocumentClick);
    };
  }, [onDocumentClick, onClose]);

  useEffect((): (() => void) => {
    document.addEventListener("keydown", onKeyDown);
    return (): void => document.removeEventListener("keydown", onKeyDown);
  });

  const onKeyDown = (event: KeyboardEvent): void => {
    if (!options?.persistent) {
      event.key === "Escape" && onClose(false);
    }
  };

  return ReactDOM.createPortal(
    <div className={styles["modal"]}>
      <div className={styles["modal-content"]} ref={contentRef}>
        <div className={styles["modal-header"]}>
          {title}
          {!options?.persistent && (
            <button
              className={styles["modal-close"]}
              onClick={(): void => onClose(false)}>
              <img src={close} alt="Close" />
            </button>
          )}
        </div>
        <div className={styles["modal-body"]}>{children}</div>
      </div>
    </div>,
    document.body
  );
};

export type ModalOptions = Partial<{
  persistent: boolean;
  onClose: () => void;
  onSuccess: () => void;
  disableOutsideClick: boolean;
}>;

interface ModalContextValue {
  showModal: (
    uuid: string,
    node: React.ReactNode,
    title: string,
    options?: ModalOptions
  ) => void;
  closeModal: (uuid: string) => void;
  isOpen: (uuid: string) => boolean;
}

interface ModalConfig {
  node: React.ReactNode;
  title: string;
  options?: Partial<ModalOptions>;
  isOpen?: boolean;
}

const ModalContext = React.createContext<ModalContextValue>({
  closeModal: () => {
    void 0;
  },
  isOpen: () => false,
  showModal: () => {
    void 0;
  },
});

type ModalProviderProps = {
  children?: React.ReactNode | undefined;
};

export const ModalProvider: React.FC<{ children: React.ReactNode }> = (
  props: ModalProviderProps
) => {
  const [modalList, setModalList] = useState<{ [key: string]: ModalConfig }>(
    {}
  );

  const showModal = (
    uuid: string,
    node: ReactNode,
    title: string,
    options?: ModalOptions
  ): void => {
    if (!modalList[uuid]) {
      setModalList((modals) => ({
        ...modals,
        [uuid]: { isOpen: false, node, options: options || {}, title },
      }));
    }

    if (!modalList[uuid]?.isOpen) {
      setModalList((modals) => ({
        ...modals,
        [uuid]: { ...modals[uuid]!, isOpen: true },
      }));
    }
  };

  const closeModal = (uuid: string, callOnClose = true): void => {
    const modal = modalList[uuid];
    if (modal && modal.isOpen) {
      setModalList((modals) => ({
        ...modals,
        [uuid]: { ...modals[uuid]!, isOpen: false },
      }));
      if (modal.options?.onClose && callOnClose) {
        modal.options.onClose();
      }
    }
  };

  const isOpen = (uuid: string): boolean => {
    return (modalList[uuid] !== undefined && modalList[uuid]?.isOpen) || false;
  };

  const onSuccess = (uuid: string): void => {
    const modal = modalList[uuid];
    closeModal(uuid, false);
    if (modal && modal.isOpen && modal.options?.onSuccess) {
      modal.options.onSuccess();
    }
  };

  return (
    <ModalContext.Provider
      value={{
        closeModal,
        isOpen,
        showModal,
      }}>
      {props.children}
      {Object.keys(modalList)
        .filter((uuid) => modalList[uuid]?.isOpen)
        .map((uuid) => {
          const node = modalList[uuid]!.node;

          return (
            <ModalComponent
              key={`modal-${uuid}`}
              options={modalList[uuid]!.options}
              onClose={(outsideClick): void => {
                if (
                  modalList[uuid]?.options?.disableOutsideClick &&
                  outsideClick
                ) {
                  return;
                }
                closeModal(uuid);
              }}
              title={modalList[uuid]!.title}>
              {React.isValidElement(node) &&
                React.cloneElement(node as React.ReactElement<ModalOptions>, {
                  onClose: () => {
                    closeModal(uuid, true);
                  },
                  onSuccess: () => {
                    onSuccess(uuid);
                  },
                })}
            </ModalComponent>
          );
        })}
    </ModalContext.Provider>
  );
};

export interface ModalHook {
  showModal: () => void;
  closeModal: () => void;
  isOpen: boolean;
}

export const useModal = (
  title: string,
  node: ReactNode,
  options?: ModalOptions
): ModalHook => {
  const { showModal, closeModal, isOpen } = useContext(ModalContext);
  const ref = useRef<ReactNode>(node);
  const uuid = "";

  useEffect(() => {
    ref.current = node;
  }, [node]);

  const presentModal = useCallback(() => {
    showModal(uuid, ref.current, title, options);
  }, [uuid, title, ref, showModal, options]);

  const unpresentModal = useCallback(() => {
    closeModal(uuid);
  }, [closeModal, uuid]);

  const isModalOpen = useMemo(() => isOpen(uuid), [isOpen, uuid]);

  return {
    closeModal: unpresentModal,
    isOpen: isModalOpen,
    showModal: presentModal,
  };
};
