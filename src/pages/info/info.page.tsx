/* eslint-disable max-len */
import React from 'react';
import styles from './info.module.scss';
import Skillbased from '../../../public/assets/images/skillbased.jpg';
import { IllustratedTextListComponent } from '../../components/illustrated-text-list/IllustratedTextList.component';

export const Info: React.FC = () => {
  return (
    <div className={styles['info']}>
      <IllustratedTextListComponent items={illustratedTextItems}/>
    </div>
  );
};

const snowwhite = 'Once upon a time in the middle of winter, when the flakes of snow were falling like feathers from the sky, a queen sat at a window sewing, and the frame of the window was made of black ebony. And whilst she was sewing and looking out of the window at the snow, she pricked her finger with the needle, and three drops of blood fell upon the snow. And the red looked pretty upon the white snow, and she thought to herself, "Would that I had a child as white as snow, as red as blood, and as black as the wood of the window-frame."'

const illustratedTextItems = [
  {
    image: Skillbased,
    title: "Goldengoose",
    text: snowwhite
  },
];
