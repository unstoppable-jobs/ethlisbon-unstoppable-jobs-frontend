/* eslint-disable max-len */
import React, { useEffect, useState } from "react";
import { ButtonComponent } from "../../components/button/Button.component";
import styles from "./jobs.module.scss";
import { Web3Button } from "@web3modal/react";
import { useMenu } from "../../hooks/useMenu";
import { SpinnerComponent } from "../../components/spinner/Spinner.component";
import {
  Talent,
  TalentComponent,
} from "../../components/talent/talent.component";
import {
  Job,
  JobItemsComponent,
} from "../../components/jobs/job-items.component";

export const Jobs: React.FC = () => {
  const {
    walletConnected,
    walletAddress,
    allJobIDs: allJobIDs,
    allTalentIDs: allTalentIDs,
    matchJobWithTalentResult: matchJobWithTalentResult,
    matchTalentWithJobsResult: matchTalentWithJobsResult,
    isLoading,
    user,
  } = useMenu();

  const [pageState, setPageState] = useState<number>(0);

  const [talentItems, setTalentItems] = useState<Talent[]>();

  const [jobItems, setJobItems] = useState<Job[]>();

  useEffect(() => {
    console.log("get all nftWithSkills");
    console.log("user.accountType", user?.accountType);
    console.log("user.nfts", user?.nfts);
  }, [user]);

  useEffect(() => {
    console.log("matchJobWithTalentResult", matchJobWithTalentResult);
  }, [matchJobWithTalentResult]);

  useEffect(() => {
    console.log("matchTalentWithJobsResult", matchTalentWithJobsResult);
  }, [matchTalentWithJobsResult]);

  useEffect(() => {
    console.log("show all job ids");
    console.log(allJobIDs);
    console.log("show all talent ids");
    console.log(allTalentIDs);
  }, [allJobIDs, allTalentIDs]);

  useEffect(() => {
    if (isLoading) {
      setPageState(7);
    } else if (!walletConnected && !walletAddress) {
      setPageState(9);
    } else if (walletConnected && walletAddress && user) {
      if (user.accountType === "talent") {
        setTalentItems(
          user.nfts.map((nft) => ({
            title: nft.id.toString(),
            description: nft.uri,
          }))
        );
        setPageState(2);
      } else if (user.accountType === "job") {
        setJobItems(
          user.nfts.map((nft) => ({
            title: nft.id.toString(),
            description: nft.uri,
          }))
        );
        setPageState(5);
      } else if (user.accountType === "" || undefined) {
        setPageState(0);
      }
    }
  }, [walletConnected, walletAddress, user, isLoading]);

  return (
    <div className={styles["jobs"]}>
      {pageState === 0 && (
        <>
          <div className={styles["choice"]}>
            <div className={styles["choice-title"]}>
              Please Choose if you want to find a job or find Talent
            </div>
            <div className={styles["two-buttons-choice"]}>
              <ButtonComponent
                text={"Find Talent"}
                onClick={(): void => setPageState(1)}
              />
              <ButtonComponent
                text={"Find Jobs"}
                onClick={(): void => setPageState(2)}
              />
            </div>
          </div>
        </>
      )}

      {/* PageStates
    CHOOSING: 0;
    FIND_TALENT: 1;
    DISPLAY_TALENT: 2;
    MINT_TALENT: 3;

    FIND_JOBS: 4;
    DISPLAY_JOBS: 5;
    MINT_JOBS: 6

    LOADING: 7;
    ERROR: 8;
    UNNCONNECTED: 9; */}

      {pageState === 0 && ( // CHOOSING
        <>
          <div className={styles["find-talent"]}>
            <div className={styles["top-title"]}>
              Do you want to list your profile or are you an employer that wants
              to list a job
            </div>
          </div>
        </>
      )}
      {pageState === 1 && ( // FIND_TALENT
        <>
          <div className={styles["find-talent"]}>
            <div className={styles["top-title"]}>Find Talent</div>
          </div>
        </>
      )}
      {pageState === 2 && ( // DISPLAY_TALENT
        <>
          <div className={styles["find-jobs"]}>
            <div className={styles["top-title"]}>
              Show my personal skills NFT
            </div>
            <TalentComponent items={talentItems} />
          </div>
        </>
      )}
      {pageState === 3 && ( // MINT_TALENT
        <>
          <div className={styles["find-jobs"]}>
            <div className={styles["top-title"]}>
              Mint Your Personal Skills NFT
            </div>
          </div>
        </>
      )}
      {pageState === 4 && ( // FIND_JOBS
        <>
          <div className={styles["find-jobs"]}>
            <div className={styles["top-title"]}>Find jobs</div>
          </div>
        </>
      )}
      {pageState === 5 && ( // DISPLAY_JOBS
        <>
          <div className={styles["find-jobs"]}>
            <div className={styles["top-title"]}>List jobs</div>
          </div>
          <JobItemsComponent items={jobItems} />
        </>
      )}
      {pageState === 6 && ( // MINT_JOBS
        <>
          <div className={styles["find-jobs"]}>
            <div className={styles["top-title"]}>Mint jobs</div>
          </div>
        </>
      )}

      {pageState === 7 && ( // LOADING
        <>
          <div className={styles["find-jobs"]}>
            <div className={styles["top-title"]}>Loading</div>
            <SpinnerComponent />
          </div>
        </>
      )}
      {pageState === 8 && ( // ERROR
        <>
          <div className={styles["find-jobs"]}>
            <div className={styles["top-title"]}>Error</div>
          </div>
        </>
      )}
      {pageState === 9 && ( // UNNCONNECTED
        <>
          <div className={styles["unconnected"]}>
            <div className={styles["top-title"]}>
              Please connect your wallet to continue
            </div>
            <div className={styles["connect-button"]}>
              <Web3Button />
            </div>
          </div>
        </>
      )}
    </div>
  );
};
