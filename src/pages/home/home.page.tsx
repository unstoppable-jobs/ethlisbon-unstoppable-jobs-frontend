/* eslint-disable max-len */
import React from 'react';
import styles from './home.module.scss';
import Hiringbias from '../../../public/assets/images/hiringbias.jpg';
import Skillbased from '../../../public/assets/images/skillbased.jpg';
import Personalbias from '../../../public/assets/images/personalbias.jpg';
import { IllustratedTextListComponent } from '../../components/illustrated-text-list/IllustratedTextList.component';

export const Home: React.FC = () => {
  return (
    <div className={styles['home']}>
      <IllustratedTextListComponent items={illustratedTextItems}/>
    </div>
  );
};

const unstoppable = `
  Unstoppable Jobs adds a layer of anonymity on top of the hiring process.
  It becomes a wallet to wallet process.
  No middle man can interfer or alter the interaction.
  By directly connecting parties hefty fees can be avoided.
`;

const unbiassed = `
  Unstoppable Jobs is a platform that is free from bias. 
  Too often hiring takes place on the bases of ethnicity, race or sex.
  Using matching algorithms that do not include any of these factors ensures a fair hiring process.
`;

const skillbased = `
  Unstoppable Jobs matching algorithm is based on skills only.
  This ensures that the best candidate is selected for the job.
  No more hiring on the bases of who you know or who you like.
`;

const illustratedTextItems = [
  {
    image: Hiringbias,
    title: "Unstoppable",
    text: unstoppable
  },
  {
    image: Personalbias,
    title: "Unbiased",
    text: unbiassed
  },
  {
    image: Skillbased,
    title: "Skillbased",
    text: skillbased
  }
];
