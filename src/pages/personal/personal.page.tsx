/* eslint-disable @typescript-eslint/no-misused-promises */
/* eslint-disable max-len */
import React, { ChangeEvent, ChangeEventHandler, useState } from "react";
import styles from "./personal.module.scss";
import { ButtonComponent } from "../../components/button/Button.component";
import { create, CID, IPFSHTTPClient } from "ipfs-http-client";

import img from "./../../ipfsfiles/unstoppable.png";
import { taikaiSkills } from "../../utils/taikai-skills";

import myjson from "./../../ipfsfiles/myFirstJSON.json";

export interface formState {
  typeNFT: string;
  imageURI: string;
  skill1: string;
  skill2: string;
  skill3: string;
  skill4: string;
  skill5: string;
  skill6: string;
  skill7: string;
  skill8: string;
  skill9: string;
}

export const Personal: React.FC = () => {
  let ipfs: IPFSHTTPClient | undefined;
  try {
    const projectId = "2GqVcIsVN5Nuac2ytLvGDK7ftwz";
    const projectSecret = "7c93a0cfab497099fcccdb0dd59d6d6f";
    const authorization = "Basic " + btoa(projectId + ":" + projectSecret);

    ipfs = create({
      // url: "https://ipfs.infura.io:5001/api/v0/",
      url: "https://ipfs.infura.io:5001",
      headers: { authorization },
    });
    console.log("ipfs: ", ipfs);
  } catch (error) {
    console.error("log my IPFS error ", error);
    ipfs = undefined;
  }

  const [currentForm, setCurrentForm] = useState<formState>({} as formState);

  const [files, setImages] = useState<{ cid: CID; path: string }[]>([]);

  const handleChange: ChangeEventHandler<
    HTMLInputElement | HTMLSelectElement
  > = (event) => {
    setCurrentForm({ ...currentForm, [event.target.name]: event.target.value });
  };

  // const json: any = myjson;
  const onSubmitHandler = async (event: React.FormEvent): Promise<void> => {
    event.preventDefault();

    const objFromForm = {
      title: "Unstoppable Jobs",
      typeNFT: "talent",
      properties: {
        typeNFT: {
          type: "string",
          description: currentForm.typeNFT,
        },
        skill1: {
          type: "string",
          description: currentForm.skill1,
        },
        image: {
          type: "string",
          description: currentForm.imageURI,
        },
      },
    };

    const jsonBlob = new Blob([JSON.stringify(objFromForm)], {
      type: "text/json",
    });

    const result = await (ipfs as IPFSHTTPClient).add(jsonBlob);
    console.log("result: ", result);

    return;
  };
  const pushToIPFS = async (): Promise<void> => {
    if (!myjson) {
      return alert("No file selected");
    }
    const jsonBlob = new Blob([JSON.stringify(myjson)], { type: "text/json" });
    const result = await (ipfs as IPFSHTTPClient).add(jsonBlob);

    setImages([...files, { cid: result.cid, path: result.path }]);
  };

  return (
    <div className={styles["personal"]}>
      <div className={styles["personal__content"]}>
        {!ipfs && <div>IPFS not connected</div>}
        {ipfs && <h1>IPFS connected, you can now upload your profile</h1>}
        {/* <ButtonComponent
          onClick={async (): Promise<void> => {
            await pushToIPFS();
          }}>
          Mint And Publish Profile to IPFS
        </ButtonComponent> */}
        <h1> Please choose your skills below</h1>
        {ipfs && (
          <>
            {files.map((file, index) => (
              <div key={index}>{`https://ipfs.infura.io/ipfs/${file.cid}`}</div>
            ))}
          </>
        )}
      </div>
      <form onSubmit={onSubmitHandler}>
        <label htmlFor="typeNFT">
          <h2>Please choose talent or job</h2>
        </label>
        <br />
        <input
          type="text"
          id="typeNFT"
          name="typeNFT"
          onChange={handleChange}
        />
        <label htmlFor="imageURI">
          <h2>Please choose ImageURI</h2>
        </label>
        <input
          type="text"
          id="imageURI"
          name="imageURI"
          onChange={handleChange}
        />
        <br />
        <select name="skill1" id="skill1" onChange={handleChange}>
          {taikaiSkills.data.technicalSkills.map((skill, index) => (
            <option key={index} value={skill?.title}>
              {skill.title}
            </option>
          ))}
        </select>
        <select name="skill2" id="skill2" onChange={handleChange}>
          {taikaiSkills.data.technicalSkills.map((skill, index) => (
            <option key={index} value={skill?.title}>
              {skill.title}
            </option>
          ))}
        </select>
        <select name="skill3" id="skill3" onChange={handleChange}>
          {taikaiSkills.data.technicalSkills.map((skill, index) => (
            <option key={index} value={skill?.title}>
              {skill.title}
            </option>
          ))}
        </select>
        <select name="skill4" id="skill4" onChange={handleChange}>
          {taikaiSkills.data.technicalSkills.map((skill, index) => (
            <option key={index} value={skill?.title}>
              {skill.title}
            </option>
          ))}
        </select>
        <select name="skill5" id="skill5" onChange={handleChange}>
          {taikaiSkills.data.technicalSkills.map((skill, index) => (
            <option key={index} value={skill?.title}>
              {skill.title}
            </option>
          ))}
        </select>
        <select name="skill6" id="skill6" onChange={handleChange}>
          {taikaiSkills.data.technicalSkills.map((skill, index) => (
            <option key={index} value={skill?.title}>
              {skill.title}
            </option>
          ))}
        </select>
        <select name="skill7" id="skill7" onChange={handleChange}>
          {taikaiSkills.data.technicalSkills.map((skill, index) => (
            <option key={index} value={skill?.title}>
              {skill.title}
            </option>
          ))}
        </select>
        <select name="skill8" id="skill8" onChange={handleChange}>
          {taikaiSkills.data.technicalSkills.map((skill, index) => (
            <option key={index} value={skill?.title}>
              {skill.title}
            </option>
          ))}
        </select>
        <select name="skill9" id="skill9" onChange={handleChange}>
          {taikaiSkills.data.technicalSkills.map((skill, index) => (
            <option key={index} value={skill?.title}>
              {skill.title}
            </option>
          ))}
        </select>
        <select name="skill10" id="skill10" onChange={handleChange}>
          {taikaiSkills.data.technicalSkills.map((skill, index) => (
            <option key={index} value={skill?.title}>
              {skill.title}
            </option>
          ))}
        </select>
        <button type="submit">Submit</button>
      </form>
    </div>
  );
};
