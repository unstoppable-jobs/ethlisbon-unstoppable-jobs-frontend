import React from "react";
import { Home } from "./home/home.page";

import hom from "../../public/assets/images/svg/home.svg";
import bolt from "../../public/assets/images/svg/bolt.svg";

import { Jobs } from "./jobs/jobs.page";
import { Info } from "./info/info.page";
import { Personal } from "./personal/personal.page";

export enum PageKeys {
  Home = 1,
  Jobs = 2,
  Info = 3,
  Personal = 4,
}

export interface Page {
  index: number;
  key: PageKeys;
  title: string;
  icon: string | undefined;
  component: React.ReactElement;
}

export const pages: Page[] = [
  {
    index: 1,
    key: PageKeys.Home,
    title: "Home",
    icon: hom,
    component: <Home />,
  },
  {
    index: 2,
    key: PageKeys.Jobs,
    title: "Jobs",
    icon: bolt,
    component: <Jobs />,
  },
  {
    index: 3,
    key: PageKeys.Info,
    title: "Info",
    icon: hom,
    component: <Info />,
  },
  {
    index: 3,
    key: PageKeys.Personal,
    title: "Personal",
    icon: hom,
    component: <Personal />,
  },
];
