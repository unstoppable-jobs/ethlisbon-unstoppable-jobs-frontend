export const network = process.env.CONTRACT_ADDRESS || "";
export const web3ModalProjectId: string =
  process.env.WEB3_MODAL_PROJECT_ID || "";
export const contractAddress: string = process.env.CONTRACT_ADDRESS || "";
export const rpc: string = process.env.GOERLI_RPC_URL || "";
