/* eslint-disable max-len */
import { network } from "./environment.config";

import Ethereum from "../../public/assets/images/svg/blockchains/ethereum.svg";

import USDC from "../../public/assets/images/svg/tokens/usdc.svg";
import USDT from "../../public/assets/images/svg/tokens/usdt.svg";

export interface Coin extends NativeCoin {
  address: string;
}

export interface NativeCoin {
  name: string;
  symbol: string;
  icon: string;
  decimals: number;
}

export interface Network {
  name: string;
  icon: string;
  rpc: string;
  chainId: number;
  blockExplorerUrls: string[];
  active: boolean;
  contractAddress?: string;
  nativeCurrency?: NativeCoin;
  supportedStableCoins?: Coin[];
}

export function getNetwork(chainId: number | undefined): Network {
  if (!chainId) {
    throw new Error("No chainId specified");
  }
  const network = networks.find((n) => n.chainId === chainId);
  if (!network) {
    throw new Error(
      "Unable to find Network with chainId: " + chainId.toString()
    );
  }
  return network;
}

const getRandomRPC = (rpcString: string): string => {
  const rpcList = rpcString.split(",");
  const index = Math.floor(Math.random() * rpcList.length);
  return rpcList[index] as string;
};

const getSupportedChainIds = (): number[] => {
  const chainsIds: number[] = [];
  const splitted = network.split("|");
  splitted.forEach((chainId) => {
    if (chainId && chainId != "" && chainId != undefined) {
      chainsIds.push(Number(chainId));
    }
  });
  return chainsIds;
};

export const chainIds = getSupportedChainIds();

export const networks: Network[] = [
  {
    name: "Ethereum",
    icon: Ethereum,
    rpc: getRandomRPC(
      "https://mainnet.infura.io/v3/0c94aec7289f455ab6dd1aa270acce0c"
    ),
    chainId: 1,
    active: chainIds.indexOf(1) > 0,
    contractAddress: "0x0",
    blockExplorerUrls: [""],
    supportedStableCoins: [
      {
        name: "USDT",
        symbol: "USDT",
        address: "0xdac17f958d2ee523a2206206994597c13d831ec7",
        icon: USDT,
        decimals: 6,
      },
      {
        name: "USDC",
        symbol: "USDC",
        address: "0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48",
        icon: USDC,
        decimals: 6,
      },
    ],
  },
  {
    name: "Goerli",
    icon: Ethereum,
    rpc: getRandomRPC(
      "https://goerli.infura.io/v3/389b211f4a804b70a571794958544829"
    ),
    chainId: 4,
    active: chainIds.indexOf(4) > 0,
    contractAddress: "0x0",
    blockExplorerUrls: [""],
    supportedStableCoins: [
      {
        name: "USDC",
        symbol: "USDC",
        address: "0x0",
        icon: USDC,
        decimals: 18,
      },
    ],
  },
];
