import {
  BigNumber,
  Contract,
  ContractInterface,
  providers,
  utils,
} from "ethers";
import unstoppableJobsABI from "../../contracts/UnstoppableJobs.sol/UnstoppableJobs.json";
import { contractAddress, rpc } from "../config/environment.config";
import { UnstoppableJobs } from "./../../types/typechain-types/UnstoppableJobs";

export interface NftWithSkills {
  id: number;
  uri: string | undefined;
  skills?: string[];
}

export interface Matches {
  ids: number[];
  uris: string[];
  rating: number[];
}

export const getUnstoppableJobsContract = (): UnstoppableJobs => {
  return getContract<UnstoppableJobs>(unstoppableJobsABI.abi, contractAddress);
};

type JsonRpcErrorData =
  | { message: string }
  | { originalError: { message: string } };

export type JsonRpcError = {
  code: number;
  message: string;
  data?: JsonRpcErrorData;
};

export const getContract = <T extends Contract>(
  abi: ContractInterface,
  address: string
): T => {
  const provider = getProvider();
  return new Contract(address, abi).connect(provider) as T;
};

export const extractRevertReason = (error: JsonRpcError): string => {
  const message =
    (error.data as { message: string })?.message ||
    (error.data as { originalError: { message: string } })?.originalError
      ?.message ||
    error.message;

  if (message) {
    return (
      // eslint-disable-next-line max-len
      /(Error: VM Exception while processing transaction: reverted with reason string|execution reverted:|MetaMask Tx Signature:) (.*)/
        .exec(message)?.[2]
        ?.replace(/'/g, "")
        .trim() || "Unknown revert reason"
    );
  }
  return "Unknown revert reason";
};

export const getProvider = (): providers.Provider => {
  return new providers.JsonRpcProvider(rpc);
};

export const convertToRelativeNumber = (number: BigNumber): number => {
  return Number(utils.formatUnits(number, 36));
};

export const getAccountType = async (address: string): Promise<string> => {
  return getUnstoppableJobsContract().accountType(address);
};

export const getAccountNfts = async (
  address: string
): Promise<NftWithSkills[]> => {
  const [ids, uris] = await getUnstoppableJobsContract().getAllNFTsFromAddress(
    address
  );
  return ids.map((id, index) => {
    return { id: id.toNumber(), uri: uris[index] };
  });
};

export const getAllJobIDs = async (): Promise<number[]> => {
  const jobIDs = await getUnstoppableJobsContract().getJobIDs();
  return jobIDs.map((id: BigNumber) => id.toNumber());
};

export const getAllTalentIDs = async (): Promise<number[]> => {
  const talentIDs = await getUnstoppableJobsContract().getTalentIDs();
  return talentIDs.map((id: BigNumber) => id.toNumber());
};

export const getMatchJobWithTalent = async (
  jobID: number
): Promise<Matches | undefined> => {
  const matches = (await getUnstoppableJobsContract().matchJobWithTalent(
    jobID
  )) as unknown as Matches;
  const { ids, uris, rating } = matches;
  return { ids, uris, rating };
};

export const getMatchTalentWithJobs = async (
  talentID: number
): Promise<Matches | undefined> => {
  const matches = (await getUnstoppableJobsContract().matchTalentWithJobs(
    talentID
  )) as unknown as Matches;
  const { ids, uris, rating } = matches;
  return { ids, uris, rating };
};

export const getSkillsFromNFT = async (nftID: number): Promise<string[]> => {
  return getUnstoppableJobsContract().getSkills(nftID);
};
