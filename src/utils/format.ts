export const formatNumbersWithDotDelimiter = (value: number): string => {
  return new Intl.NumberFormat("en-US").format(value);
};

export const formatNumbersWithAbbreviation = (value: number): string | void => {
  if (value > 999 && value < 1000000) {
    return (value / 1000).toFixed(0) + "K";
  } else if (value >= 1000000) {
    return (value / 1000000).toFixed(0) + "M";
  } else if (value < 900) {
    return String(value);
  }
};
