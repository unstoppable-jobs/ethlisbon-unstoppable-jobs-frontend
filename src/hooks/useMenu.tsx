import { useAccount } from "@web3modal/react";
import React, { useContext, useEffect, useMemo, useState } from "react";
import { PageKeys } from "../pages/pages";
import styles from "../../public/assets/styles/_variables/_breakpoints.scss";
import {
  getAccountType,
  getAllJobIDs,
  getAllTalentIDs,
  Matches,
  getMatchJobWithTalent,
  getMatchTalentWithJobs,
  getSkillsFromNFT,
  NftWithSkills,
  getAccountNfts,
} from "../utils/blockchain";

export interface User {
  accountType: string;
  nfts: NftWithSkills[];
}

interface MenuValue {
  walletConnected: boolean;
  walletAddress: string | undefined;
  allJobIDs: number[];
  allTalentIDs: number[];
  matchJobWithTalentResult: Matches;
  matchTalentWithJobsResult: Matches;
  isLoading: boolean;
  isMobile: boolean;
  setIsMobile: (isMobile: boolean) => void;
  selectedPage: PageKeys;
  onPageSelected: (tab: PageKeys) => void;
  isHamburgerOpen: boolean;
  onHamburgerToggled: () => void;
  user: User | undefined;
}

const MenuContext = React.createContext<MenuValue>({
  walletConnected: false,
  walletAddress: undefined,
  allJobIDs: [],
  allTalentIDs: [],
  matchJobWithTalentResult: {} as Matches,
  matchTalentWithJobsResult: {} as Matches,
  isLoading: false,
  isMobile: false,
  setIsMobile: () => {
    void 0;
  },
  selectedPage: PageKeys.Home,
  onPageSelected: () => {
    void 0;
  },
  isHamburgerOpen: false,
  onHamburgerToggled: () => {
    void 0;
  },
  user: undefined,
});

export const useMenu = (): MenuValue => useContext(MenuContext);

type MenuProviderProps = {
  children: React.ReactNode;
};

export const MenuProvider: React.FC<MenuProviderProps> = (
  props: MenuProviderProps
) => {
  const [selectedPage, setSelectedPage] = useState<PageKeys>(PageKeys.Home);
  const [isMobile, setIsMobile] = useState<boolean>(false);
  const [isHamburgerOpen, setIsHamburgerOpen] = useState<boolean>(false);

  const [allJobIDs, setAllJobIDs] = useState<number[]>([]);
  const [allTalentIDs, setAllTalentIDs] = useState<number[]>([]);

  const [fetchedJobWithTalentResult, setFetchedJobWithTalentResult] =
    useState<Matches>({} as Matches);

  const [fetchedTalentWithJobsResult, setFetchedTalentWithJobsResult] =
    useState<Matches>({} as Matches);

  const [user, setUser] = useState<User>();

  const [isLoading, setIsLoading] = useState<boolean>(false);

  const { account } = useAccount();

  //This is the ID for which the Matches are being fetched
  const [idToMatch, setIdToMatch] = useState<number>(3);

  const cachedUser = useMemo(async () => {
    if (account.address) {
      setIsLoading(true);
      const accountType = await getAccountType(account.address);
      const nfts = await getAccountNfts(account.address);
      for (const nft of nfts) {
        nft.skills = await getSkillsFromNFT(nft.id);
      }
      setUser({ accountType, nfts });
      setIsLoading(false);
    }
  }, [account.address]);

  useEffect(() => {
    const fetchJobIDs = async (): Promise<void> => {
      const a = await getAllJobIDs();
      setAllJobIDs(a);
    };
    const fetchTalentIDs = async (): Promise<void> => {
      const a = await getAllTalentIDs();
      setAllTalentIDs(a);
    };
    void fetchJobIDs();
    void fetchTalentIDs();
  }, []);

  const cachedMatchJobWithTalentResult = useMemo(async () => {
    if (idToMatch) {
      return await getMatchJobWithTalent(idToMatch);
    }
    return;
  }, [idToMatch]);

  useEffect(() => {
    const getMatchJobWithTalentResult = async (): Promise<void> => {
      const result = (await cachedMatchJobWithTalentResult) as Matches;
      if (result) {
        setFetchedJobWithTalentResult(result);
      }
    };
    void getMatchJobWithTalentResult();
  }, [cachedMatchJobWithTalentResult]);

  const cachedMatchTalentWithJobsResult = useMemo(async () => {
    if (idToMatch) {
      return await getMatchTalentWithJobs(idToMatch);
    }
    return;
  }, [idToMatch]);

  useEffect(() => {
    const getMatchTalentWithJobsResult = async (): Promise<void> => {
      const result = (await cachedMatchTalentWithJobsResult) as Matches;
      if (result) {
        setFetchedTalentWithJobsResult(result);
      }
    };
    void getMatchTalentWithJobsResult();
  }, [cachedMatchTalentWithJobsResult]);

  const onHamburgerToggled = (): void => {
    setIsHamburgerOpen(!isHamburgerOpen);
  };

  const onSetMobile = (isMobile: boolean): void => {
    setIsMobile(true);
  };

  const onPageSelected = (tab: PageKeys): void => {
    setIsHamburgerOpen(false);
    setSelectedPage(tab);
  };

  useEffect(() => {
    const resizeObserver = new ResizeObserver(() => {
      if (window.innerWidth <= Number(styles["mobile-max-size"])) {
        setIsMobile(true);
      } else {
        setIsMobile(false);
      }
      resizeObserver.observe(document.querySelector("body") as Element);
    });
  }, [setIsMobile]);

  return (
    <MenuContext.Provider
      value={{
        walletConnected: account.isConnected || false,
        walletAddress: account.address,
        allJobIDs: allJobIDs,
        allTalentIDs: allTalentIDs,
        matchJobWithTalentResult: fetchedJobWithTalentResult,
        matchTalentWithJobsResult: fetchedTalentWithJobsResult,
        isLoading,
        isMobile,
        setIsMobile,
        selectedPage,
        onPageSelected,
        isHamburgerOpen,
        onHamburgerToggled,
        user,
      }}>
      {props.children}
    </MenuContext.Provider>
  );
};
